package university.jala.lab8.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import university.jala.lab8.domain.entity.UsuarioMongo;
import university.jala.lab8.domain.entity.UsuarioSql;
import university.jala.lab8.service.MigracaoService;

import java.util.List;

@Controller
@RequestMapping("/api")
public class MigracaoController {

    @Autowired
    private MigracaoService migracaoService;

    @GetMapping("/listar/usuarios/sql")
    public ResponseEntity<List <UsuarioSql>> controllerListarUsuariosSql() {
        return migracaoService.listarUsuariosSql();
    }

    @GetMapping("listar/usuarios/mongo")
    public ResponseEntity<List <UsuarioMongo>> controllerListarUsuariosMongo() {
        return migracaoService.listarUsuariosMongo();
    }

    @GetMapping("/migrar")
    public ResponseEntity<String> controllerMigracao() {
        return migracaoService.migrarDados();
    }
}
