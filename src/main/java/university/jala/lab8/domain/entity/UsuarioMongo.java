package university.jala.lab8.domain.entity;


import jakarta.persistence.Id;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "usuario")
@Getter
@Setter
public class UsuarioMongo {
    @Id
    private String id;
    private String nome;
    private String login;
    private String senha;
}
