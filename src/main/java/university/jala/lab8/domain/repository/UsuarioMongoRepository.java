package university.jala.lab8.domain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import university.jala.lab8.domain.entity.UsuarioMongo;

public interface UsuarioMongoRepository extends MongoRepository<UsuarioMongo, String> {
}
