package university.jala.lab8.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import university.jala.lab8.domain.entity.UsuarioSql;

public interface UsuarioSqlRepository extends JpaRepository<UsuarioSql, String> {
}
