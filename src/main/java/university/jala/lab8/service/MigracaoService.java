package university.jala.lab8.service;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import university.jala.lab8.domain.entity.UsuarioMongo;
import university.jala.lab8.domain.entity.UsuarioSql;
import university.jala.lab8.domain.repository.UsuarioMongoRepository;
import university.jala.lab8.domain.repository.UsuarioSqlRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class MigracaoService {

    private UsuarioMongoRepository usuarioMongoRepository;
    private UsuarioSqlRepository usuarioSqlRepository;

    public ResponseEntity<List <UsuarioMongo>> listarUsuariosMongo() {
        if (usuarioMongoRepository.findAll().isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(usuarioMongoRepository.findAll());
    }

    public ResponseEntity<List <UsuarioSql>> listarUsuariosSql() {
        if (usuarioSqlRepository.findAll().isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(usuarioSqlRepository.findAll());
    }

    public ResponseEntity<String> migrarDados() {
        List<UsuarioSql> usuarios = usuarioSqlRepository.findAll();
        try {
            for (UsuarioSql usuario : usuarios) {
                UsuarioMongo usuarioMongo = new UsuarioMongo();
                usuarioMongo.setNome(usuario.getNome());
                usuarioMongo.setLogin(usuario.getLogin());
                usuarioMongo.setSenha(usuario.getSenha());

                usuarioMongoRepository.save(usuarioMongo);
            }
            return ResponseEntity.ok("Dados migrados com sucesso!");
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao migrar dados", e);
        }
    }
}
